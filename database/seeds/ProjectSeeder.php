<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('project')->insert([
            'pro_name' => 'CBBank',
            'pro_description' => 'This is CBBank project',
            'pro_own' => 2,
        ]);
    }
}
