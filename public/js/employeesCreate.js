$(document).ready(function(){
    $('.editEmp .avatar button').click(function(){
        $(this).parent().find('#fileInput').click();
    });

    $(".editEmp #fileInput").change(function () {
        //readURL(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var imgChange = $(this).parent().find('img');

            reader.onload = function (e) {
                imgChange.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    /* Choose Image */
    $('.addNew .avatar button').click(function(){
        $(this).parent().find('#imgAvatar').click();
    });

    $(".addNew #imgAvatar").change(function () {
        //readURL(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var imgChange = $(this).parent().find('img');

            reader.onload = function (e) {
               imgChange.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

});
