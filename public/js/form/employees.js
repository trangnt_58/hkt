$('document').ready(function(){
    $("#formEmp").validate({
        rules: {
            emp_name: {
                required: true,
                minlength: 3
            },
            emp_job: {
                required: true,
                minlength: 3
            },
            emp_phone: {
                required: true,
                minlength: 3
            },
            emp_email: {
                required: true,
                email: true
            },
            emp_address: {
                required: true
            }
        }
    });
});