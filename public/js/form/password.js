$('document').ready(function(){
    $.validator.addMethod("pass_not_same", function(value, element) {
        return $('input[name="old_password"]').val() != $('input[name="password"]').val()
    }, "Old and new password should not match");

    $('#changeForm').validate({
        rules: {
            old_password: {
                required: true,
                minlength: 6,
                pass_not_same: true
            },
            password: {
                required: true,
                minlength: 6,
                pass_not_same: true

            },
            again_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            }
        },
        messages: {
            again_password: {
                equalTo: 'Confirm password does not match'
            }
        }
    });

});