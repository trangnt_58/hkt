$('document').ready(function(){
    $(".addNewAdmin").validate({
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            email: {
                required: true,
                email: true
            }
        }
    });
});