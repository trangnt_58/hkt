$(document).ready(function(){
	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$( ".input" ).focusin(function() {
		$( this ).find( "span" ).animate({"opacity":"0"}, 200);
	});

	$( ".input" ).focusout(function() {
		$( this ).find( "span" ).animate({"opacity":"1"}, 300);
	});

	$('#password').keyup(function(){
		if ($('#username').val() == '') {
			$('.error_username').html('Please enter email');
		}else{
			$('.error_username').html('&nbsp');
		};

		if ($(this).val() != '') {
			$('.error_password').html('&nbsp');
		}
	});

	$('#username').keyup(function(){
		if ($(this).val() != '') {
			$('.error_username').html('&nbsp');
		}
	});
	function checkLength(str,min){
		if(str.length < min) return false;
		else return true;
	}


	$("#formLogin").submit(function(event){
		var okie = true;
		if ($('#username').val() == '') {
			$('.error_username').html('Please enter email');
			okie = false;
		}else{
			if(!validateEmail($('#username').val())){
				$('.error_username').html('Email is invalid');
				okie = false
			}else {
				$('.error_username').html('&nbsp');
			}
		}


		if($('#password').val().length < 6) {
			okie = false;
			if ($('#password').val() == ''){
				$('.error_password').html('Please enter password');
			}else{
				$('.error_password').html('Please enter more than 6 characters');
			}

		}else{
			$('.error_password').html('&nbsp');
		}
		return okie;

	});
});

