$(document).ready(function(){


    /* Update result after searching */
    var updateCountEmp = function(){
        var count = 0;

        $('#empTable tbody tr').each(function(){
            if ( $(this).css('display') != 'none'){
                count++;
            }
        });
        $('#countEmp').text(count +' result');

    }

    //Way display
    //if( $(window).width()>= 700){
    $('.gridDisplay').hide();
    $('.btnGrid').click(function(){
            $('.listDisplay').hide();
            $('.gridDisplay').show();
        }
    );
    $('.btnList').click(function(){
            $('.gridDisplay').hide();
            $('.listDisplay').show();

        }
    );
    //}

    $('.gridDisplay .action').hide();
    $('.gridDisplay .item').hover(function(){
        $(this).find('.action').toggle();
    });
    $('[data-toggle="true"]').tooltip();

});

//Sort
var sorted_col = -1; //chi so cot duoc sap xep
var sorted_order = 0; //chieu sap xep;1: tang, 2: giam
function sortcol(th) {
    var col = -1;
    // for(var col = 0; col < th.parentNode.cells.length; col++)
    // 	if(th == th.parentNode.cells[col]) break;
    for (var t=1; t < th.parentNode.cells.length; t++){
        if(th == th.parentNode.cells[t]) col = t;
        else {
            th.parentNode.cells[t].firstChild.innerHTML = "&nbsp;";
        }
    }

    //luu vet

    if( col != sorted_col) {//cot khac
        sorted_col = col;
        sorted_order = 1;
    } else {// cot cu,dao chieu cot da sap xep
        sorted_order = sorted_order==1?2:1;

    }
    // thuc hien sap xep
    //thay mui ten
    th.firstChild.innerHTML = sorted_order==1?"&uarr;":"&darr;";

    //sap xep
    var tmp = "";
    var tblrows = document.getElementById("empTable").rows;
    for(var i = 1; i < tblrows.length; i++)
        for(var j = i+1; j<tblrows.length; j++)
            if ((sorted_order == 1 && tblrows[i].cells[col].innerHTML.toLowerCase() >
                tblrows[j].cells[col].innerHTML.toLowerCase())
                ||
                (sorted_order == 2 && tblrows[i].cells[col].innerHTML.toLowerCase() <
                tblrows[j].cells[col].innerHTML.toLowerCase())
            ){
                for(var t=1; t < tblrows[i].cells.length; t++){

                    tmp = tblrows[i].cells[t].innerHTML;
                    tblrows[i].cells[t].innerHTML = tblrows[j].cells[t].innerHTML;
                    tblrows[j].cells[t].innerHTML = tmp;
                }
            }

}
