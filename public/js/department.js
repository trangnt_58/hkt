$('document').ready(function(){
	$("#formDep").validate({
		rules: {
			dep_name: {
				required: true,
				minlength: 3
			},
			dep_phone: {
				required: true,
				minlength: 8
			},
			dep_address: "required",
			mng_id: "required"
		},
		submitHandler: function(form) {
			// do other things for a valid form
			form.submit();
		}
	});
});