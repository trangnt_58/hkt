$(document).ready(function(){

	$("#formDep").validate({
		rules: {
			dep_name: "required",
			dep_phone: {required: true, minlength: 10},
			dep_address: "required",
			mng_id: "required"
		},
		messages: {
			dep_name: "Vui lòng nhập tên phòng",
			dep_phone: { required: "Vui lòng nhập số điện thoại", minlength: "Số điện thoại quá ngắn"},
			dep_address: "Vui lòng nhập địa chỉ",
			mng_id: "Vui lòng nhập mã số nhân viên"
		}
	});
});