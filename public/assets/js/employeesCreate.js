$(document).ready(function(){
    $('.editEmp .avatar button').click(function(){
        $(this).parent().find('#fileInput').click();
    });

    $(".editEmp #fileInput").change(function () {
        //readURL(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var imgChange = $(this).parent().find('img');

            reader.onload = function (e) {
                imgChange.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    /* Choose Image */
    $('.addNew .avatar button').click(function(){
        $(this).parent().find('#imgAvatar').click();
    });

    $(".addNew #imgAvatar").change(function () {
        //readURL(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var imgChange = $(this).parent().find('img');

            reader.onload = function (e) {
               imgChange.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    var isValid=true;

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function isPhone(phone) {
        var regex = /[0-9 -()+]+$/;
        return regex.test(phone);
    }
    $('input[name="emp_email"]').blur(function(){
        if(!isEmail($(this).val())){
            isValid = false;
            var label = $(this).parent().find('label').text();
            $(this).parent().find('.error').text('Invalid email');
        }else{
            isValid = true;
        }
    });
    $('input[name="emp_phone"]').blur(function(){
        if(!isPhone($(this).val()) || $(this).val().length < 6){
            isValid = false;
            var label = $(this).parent().find('label').text();
            $(this).parent().find('.error').text('Invalid phone');
        }else {
            isValid = true;
        }
    });
    //$('.form-group input[required]').parent().find('label').after('<span><sub class="markRequired">*</sub></span>');
    $('.form-group input[required]').blur(function(){
        var label = $(this).parent().find('label').text();
        if($(this).val()==''){
            $(this).parent().find('.error').text(label+' is required');
            $(this).addClass('errorInput');
            testValid();
        }
        else if($(this).val().length>=3 ){
        }else{
            $(this).parent().find('.error').text(label+' should be from more 3 characters');
        }

    });
    $('.form-group input[required]').keyup(function(){
        $(this).parent().find('.error').text('');
        $(this).removeClass('errorInput');
        testValid();
    });


    function testValid() {
        var allValid = 0;
        $('input[required]').each(function () {
            if ($(this).val() != "") {
                allValid++;
            }
        });

        if (allValid == $('input[required]').length) {
            isValid=true;
            $('button[type = "submit"]').addClass('btn-primary');
        } else {
            $('button[type = "submit"]').removeClass('btn-primary');
        }
    }

    $(".addNew form").submit(function(){
        $('input[required]').each(function() {
            var element = $(this);
            if (element.val() == "") {
                isValid = false;
                var label = $(this).parent().find('label').text();
                $(this).parent().find('.error').text(label+' is required');
                $(this).addClass('errorInput');
            }

        });
       return isValid;
    });

});
