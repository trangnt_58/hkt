-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2016 at 10:20 PM
-- Server version: 10.1.10-MariaDB-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hkt`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` int(11) NOT NULL,
  `dep_name` varchar(255) NOT NULL,
  `dep_phone` varchar(255) NOT NULL,
  `dep_address` varchar(255) NOT NULL,
  `mng_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `dep_phone`, `dep_address`, `mng_id`) VALUES
(5, 'Paris', '054545', 'Floor 1', 11),
(6, 'Jakarta', '0986565656', 'Floor 2', 11),
(10, 'London', '024545457', 'England', 18),
(11, 'Tokyo', '04555554', 'Floor 3 ', 13);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(10) UNSIGNED NOT NULL,
  `emp_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_job` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_birthday` date NOT NULL,
  `emp_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_cv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dep_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`emp_id`, `emp_name`, `emp_photo`, `emp_job`, `emp_email`, `emp_birthday`, `emp_phone`, `emp_address`, `emp_cv`, `dep_id`, `created_at`, `updated_at`) VALUES
(11, 'Demi Lovato', 'image/profilepics/1465616842.jpg', 'Coder', 'demilovato@gmail.com', '1990-05-06', '1545', 'Ha Dong, Ha Noi', 'file/cv/1465920735_demi_cv.pdf', 5, '2016-05-06 10:59:35', '2016-06-13 19:54:21'),
(13, 'Adam Levine', 'image/profilepics/1465562027.PNG', 'Coder', 'adamlevine@gmail.com', '2016-05-29', '0123456', 'Canad', '', 5, '2016-05-07 09:27:46', '2016-06-13 23:21:52'),
(14, 'Bruno Mars', 'image/profilepics/1465562040.jpg', 'Tester', 'brunomars@gmail.com', '2010-04-15', '09773552', 'American', '', 10, '2016-05-07 09:28:42', '2016-06-14 03:36:13'),
(15, 'Eminem', 'image/profilepics/1465562051.png', 'Singer', 'eminem@gmail.com', '1991-05-15', '09885454545', 'Hà Đông', '', 6, '2016-05-27 06:16:23', '2016-06-14 00:56:48'),
(18, 'Adele', 'image/profilepics/1465630523.jpg', 'Tester', 'adele@gmail.com', '1995-02-20', '578787', 'Ha Dong', '', 5, '2016-05-28 05:51:47', '2016-06-13 17:20:45'),
(21, 'Lady Gaga', '', 'Developer', 'tra@gmail.com', '1992-02-05', '5454554545', 'Ha Noi', '', 5, '2016-05-28 05:55:28', '2016-05-28 05:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `emp_task_pro`
--

CREATE TABLE `emp_task_pro` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emp_task_pro`
--

INSERT INTO `emp_task_pro` (`id`, `emp_id`, `task_id`, `pro_id`, `created_at`, `updated_at`) VALUES
(1, 11, 0, 1, NULL, NULL),
(3, 14, 0, 1, NULL, NULL),
(4, 11, 0, 2, NULL, NULL),
(6, 18, 0, 1, '2016-06-11 17:50:38', '2016-06-11 17:50:38'),
(8, 15, 0, 1, '2016-06-11 18:12:32', '2016-06-11 18:12:32'),
(9, 26, 0, 1, '2016-06-11 20:17:03', '2016-06-11 20:17:03'),
(10, 11, 1, 0, NULL, NULL),
(11, 11, 2, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_04_08_152214_create_articles_table', 2),
('2016_04_25_101903_create_employees_table', 3),
('2016_05_10_160756_add_paid_to_users', 4),
('2016_05_10_160816_add_status_to_users', 5),
('2016_06_10_000259_create_project_table', 6),
('2016_06_10_031821_create_emp_task_pro_table', 7),
('2016_06_12_165541_create_task_table', 8),
('2016_06_12_171249_create_task_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('trang2uet@gmail.com', 'f14311354b6c90184653df65f0f75c207fdb2a157266a9314ad53aac3a4170f3', '2016-04-29 04:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `pro_id` int(10) UNSIGNED NOT NULL,
  `pro_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pro_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pro_own` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`pro_id`, `pro_name`, `pro_description`, `pro_own`, `created_at`, `updated_at`) VALUES
(1, 'CBBank', 'This is CBBank project', 61, NULL, NULL),
(2, 'LaravelApp', 'This is Laravel App', 62, NULL, NULL),
(5, 'Test', 'test test test', 54, '2016-06-09 20:13:01', '2016-06-09 20:13:01'),
(6, 'Test3', 'This is test', 54, '2016-06-11 17:20:19', '2016-06-11 17:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(10) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_status` int(11) NOT NULL,
  `task_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_deadline` date NOT NULL,
  `emp_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_name`, `task_status`, `task_description`, `task_deadline`, `emp_id`, `created_at`, `updated_at`) VALUES
(1, 'Clone HKT project', 1, 'Easy', '2016-06-16', 11, NULL, NULL),
(3, 'àdsadfs', 0, '', '0000-00-00', 0, '2016-06-13 17:53:54', '2016-06-13 17:53:54'),
(4, 'dádfsfs', 0, '', '0000-00-00', 0, '2016-06-13 17:54:16', '2016-06-13 17:54:16'),
(5, 'Đi chơi', 0, '', '0000-00-00', 13, '2016-06-13 17:56:14', '2016-06-13 17:56:14'),
(6, 'Đi học đều là đi học đều', 0, '', '0000-00-00', 13, '2016-06-13 17:57:34', '2016-06-13 17:57:34'),
(52, 'Play game', 0, '', '0000-00-00', 11, '2016-06-14 12:46:59', '2016-06-14 12:46:59'),
(54, 'Swimming', 0, '', '0000-00-00', 11, '2016-06-14 12:48:17', '2016-06-14 12:48:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(54, 'Trang Nguyen', 'trang2uet@gmail.com', '$2y$10$rC8UBf.K8zesmtEXAjnBcumMx2LDj6.NU0YSaCUxBKlQKFNy4Ur/C', 'aDW5MKkYPhnigdy8mcfMOLiDA3eQ0xQGo8PYmS5SfhOncVouNWt251Xaq5fw', '2016-05-16 09:54:51', '2016-06-14 12:58:04', 1),
(61, 'Khoa Nguyen', 'khoanguyen512@gmail.com', '$2y$10$/RROERVju3B7VG5w9Z2DIOKOHjy44t2/Ptjbat480nnzZSTq5fbWG', NULL, '2016-06-10 19:36:02', '2016-06-10 19:36:02', 0),
(62, 'Doan Hien', 'helendth@gmail.com', '$2y$10$RQPPtEi5whVjehFkydUvAO4dqtfIo0crzx/ucr3anDo6pzJ3OiuBm', 'uaed4TILYzch2N6FfSbIV1Fn9OVfPeFxQoHCYRHjSbPlLcXdAQHRG5tappqr', '2016-06-10 19:36:33', '2016-06-11 00:40:27', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `emp_task_pro`
--
ALTER TABLE `emp_task_pro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `emp_task_pro`
--
ALTER TABLE `emp_task_pro`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
