@extends('layouts.app')
@section('content')
    <div class="aboutus">
        <div class="titlePage">
            <h1><i class="fa fa-trophy"></i>&nbsp;About Us</h1>
            <p class="subtitle">Get to know our team of awesome talent.</p>
        </div>
        <hr>
        <div class="row">
            <div id="member" class="col-md-4" style="height: 491px;">            
                <img src="http://tinyurl.com/h2e7wsv" class="img-circle profile" alt="Mike McAlister">
                <div class="profile-meta">
                    <h4><a href="https://www.facebook.com/trangjtwya">Trang Nguyen</a></h4>
                    <p>Designer, developer and leader. Enjoys listening to music, travelling.</p>
                </div>
            </div>
            <div id="member" class="col-md-4" style="height: 491px;">            
                <img src="http://tinyurl.com/h3lyxfa" class="img-circle profile" alt="Fizz the Cat">
                <div class="profile-meta">
                    <h4><a href="https://www.facebook.com/hiendt95">Hien Doan</a></h4>
                    <p>Designer, developer and a member of HKT team. Enjoys reading book, listening to music and eating.</p>
                </div>
            </div>
            <div id="member" class="col-md-4" style="height: 491px;">            
                <img src="http://tinyurl.com/j27u7q8" class="img-circle profile" alt="John Lennon">
                <div class="profile-meta">
                    <h4><a href="https://www.facebook.com/nguyenvankhoa.1995">Khoa Nguyen</a></h4>
                    <p>I play guitar in a little band called HKT. When I'm not coding I go out with my girl friend.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
