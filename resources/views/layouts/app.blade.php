<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hakata</title>
    <link rel="icon" type="image/x-icon" href="{!! asset('assets/img/cat.jpg') !!}" />`

    <!--  Import library -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/css/font-awesome.min.css') !!} 

   <!--  Custom style -->
    {!! Html::style('css/app.css') !!}
    {!! Html::style('css/local.css') !!}
    {!! Html::script('assets/js/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!} 
    {!! Html::script('assets/js/login.js') !!} 
    {!! Html::script('assets/js/jquery.validate.js') !!}
    {!! Html::script('assets/js/jquery.validate.min.js')!!}
    {!! Html::style('css/jquery-ui-smooth.css') !!}
    {!! Html::style('css/jquery-ui.min.css') !!}
    {!! Html::script('js/jquery-ui.min.js') !!}

</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"> <img class="imgLogo"  src="{{asset('/assets/img/t1.png')}}"/>Hakata</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="{{ Request::is( 'dashboard') ? 'active' : '' }}">
                        <a href="{{ url('/dashboard') }}"><i class="fa fa-tachometer"></i> {{trans('menu.dashboard')}}</a>
                    </li>
                    @if(Auth::check())
                    <li class="{{ Request::is( 'admin*') ? 'active' : '' }}">
                        <a href="{{ url('/admin') }}"><i class="fa fa-user-plus"></i>{{trans('menu.manage_admin')}}</a>
                    </li>
                    @endif
                    <li class="{{ Request::is( 'employees*') ? 'active' : '' }}">
                        <a href="{{ url('/employees') }}"><i class="fa fa-users"></i> {{trans('menu.employees')}}</a>
                    </li>
                    <li class="{{ Request::is( 'department*') ? 'active' : '' }}">
                        <a href="{{ url('/department')}}"><i class="fa fa-home"></i> {{trans('menu.department')}}</a>
                    </li>
                    <li class="{{ Request::is( 'project*') ? 'active' : '' }}">
                        <a href="{{ url('/project')}}"><i class="fa fa-bullseye" aria-hidden="true"></i>
                            {{trans('menu.project')}}</a>
                    </li>
                    <li class="{{Request::is('about_us') ? 'active' : ''}}">
                    	<a href="{{ url('/about_us')}}"><i class="fa fa-trophy"></i>About Us</a>
                    </li>                    
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {!! Html::image('image/icon/global.png') !!}
                        </a>
                        <ul class="dropdown-menu">
                            @foreach (Config::get('languages') as $lang => $language)
                                <li>
                                    <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @if (Auth::guest())
                        <li class="loginBtn"><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> &nbsp; Login</a></li>
                    @else
                        <li class="dropdown customDrop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {!! Html::image('image/user-icon.png') !!}
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" id="accountMenu" role="menu">
                                <li><a href="{{ url('/admin/getProfile')}}"><i class="fa fa-btn fa-file-text-o" aria-hidden="true"></i>Profile</a></li>
                                <li><a href="{{ url('/admin/password/change')}}"><i class="fa fa-btn fa-refresh" aria-hidden="true"></i>Change Password</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>

                    @endif
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            @yield('content')
        </div>
        <!-- /#page-wrapper -->
    </div>

    {{--Message Response--}}
    @if(Session::has('response_success'))
        <div class="response-mess alert alert-success action-success"><p><span class="glyphicon glyphicon-ok"></span><em> {!! session('response_success') !!}</em></p></div>
    @endif
    <script>
         $(".response-mess").animate({ width: 'show' }).delay('1200');
        $('.response-mess').animate({ width: 0 }).hide(0);
    </script>
    <footer id="footer" class="footer">
        <div class="container">
            <div class="pull-right" class="container">Copyright &copy; 2016 By HKT Team</div>
        </div>
    </footer>

</body>

</html>
