<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
<div class="addAdmin">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="panel-title">Add New Admin</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal addNewAdmin" role="form" method="POST" action="{{ url('/admin/create') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    <div class="error error_email">&nbsp;
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" id="createAdminBtn" class="btn btn-primary">
                        Submit
                    </button>
                    <button type="submit" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        $('document').ready(function(){
            $('form.addNewAdmin input[name="email"]').blur(function(){
                var isValid = true;
                var emailEnter = $(this).val();
                $.get('{{ url('/admin/getAllEmail') }}', function(data ){
                    for(var i = 0; i < data.length; i++) {
                        if (emailEnter == data[i].email) {
                            $('.error_email').text('Email already exists');
                            isValid = false;
                            break;
                        }
                    }
                    if(!isValid) {
                        $('#createAdminBtn').prop('disabled', true);
                    }else{
                        $('#createAdminBtn').prop('disabled', false);
                    }
                });
            });

            $('form.addNewAdmin input').keyup(function(){
                $(this).parent().parent().find('.error_email').text('');
            });
        });
    </script>
</div>
        </div>
    </div>
{!! Html::script('js/form/admin-add.js') !!}



