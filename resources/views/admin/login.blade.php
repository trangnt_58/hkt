<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <!--  Import library -->
  {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
  {!! Html::style('assets/bootstrap/css/bootstrap-theme.min.css') !!}
  {!! Html::style('assets/css/font-awesome.min.css') !!}
          <!--  Custom style -->
  {!! Html::style('css/login.css') !!}
  {!! Html::script('assets/js/jquery.min.js') !!}
  {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
  {!! Html::script('js/login.js') !!}

</head>

<body>
<form id="formLogin" role="form" class="login"  method="post" action="{{ url('/login') }}">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="legend">
    <div><a href="{{url('/')}}"><img class="imgLogo" src="{{asset('image/logo.png')}}"/></a> </div>
    <div class="team"><label class="nameTeam">Hakata</label></div>
  </div>
  @if ($error = $errors->first('invalid'))
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Wrong!</strong> {{ $error }}
    </div>
  @endif
  <div class="fieldForm">
    <div class="input">
      <input type="text" id="username" placeholder="Email" name="email" />
      <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
      <div class="error error_username">&nbsp;
        @if ($errors->has('email'))
          {{ $errors->first('email') }}
        @endif
      </div>

    </div>

    <div class="input">
      <input type="password" id="password" placeholder="Password" name="password" />
      <span><i class="fa fa-lock"></i></span>
      <div class="error error_password"> &nbsp;
        @if ($errors->has('password'))
          {{ $errors->first('password') }}
        @endif
      </div>
    </div>

    {{--<div class="checkbox">--}}
    {{--<label><input type="checkbox" name="remember" value="">Remember me</label>--}}
    {{--</div>--}}

    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
    <div class="goback">
      <a class="btn btn-link" href="{{ url('/') }}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
        &nbsp;Back to Homepage</a>
    </div>

  </div>
  <button type="submit" class="submit"><i class="fa fa-long-arrow-right"></i></button>
</form>

</body>
</html>

