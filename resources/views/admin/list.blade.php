@extends('layouts.app')
@section('content')
<div class="adminContainer">
    <div class="row titlePage">
        <div class="col-md-6">
            <h1 ><i class="fa fa-user-plus"></i> &nbsp;{{trans('menu.manage_admin')}}</h1>
        </div>
        <div class="col-md-6">
            <button class="btn btn-primary addNew" data-toggle="modal" data-target="#addAdmin"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add New Admin</button>
        </div>
        <!-- Modal Add Admin -->
        <div class="modal fade" id="addAdmin" role="dialog">
            @include('admin.create')
        </div>
    </div>
    <hr>
    <div class="content-page">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td><?php
                        if($user->status == 1) echo 'Actived';
                        else echo 'Not active';?>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
