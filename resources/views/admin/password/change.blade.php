@extends('layouts.app')
@section('content')
    <div class="admin-profile profileType">
        <div class="row main-profile">
            <div class="col-md-2">
                <img  src="{{asset('image/user-icon.png')}}"/>
            </div>
            <div class="col-md-10">
                <h3>{{ Auth::user()->name }}</h3>
                <p>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    {{ Auth::user()->email }}
                </p>
                <p>
                    <?php if(Auth::user()->status ==1) echo '<span class="status active" ><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;Actived</span>';
                    else echo '<span class="status" >Not Active</span>'
                    ?>
                </p>
            </div>
        </div>
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#info"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                    &nbsp;INFO</a>
            </li>
            <li class="active"><a data-toggle="tab" href="#changePassWord"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                    &nbsp;CHANGE PASSWORD</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="info" class="tab-pane fade in">
                No information
            </div>
            <div id="changePassWord" class="tab-pane fade in active">
                <form id="changeForm" class="form-horizontal" role="form" method="POST" action="{{ url('admin/password/change') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Old Password</label>
                        <div class="col-md-5">
                            <input type="password" class="form-control" name="old_password">
                            <div class="error">&nbsp;
                                @if($errors->has('old_password'))
                                    {{$errors->first('old_password')}}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">New Password</label>

                        <div class="col-md-5">
                            <input type="password" class="form-control" id="password" name="password">
                            <div class="error">&nbsp;
                                @if($errors->has('password'))
                                    {{$errors->first('password')}}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-5">
                            <input type="password"  class="form-control" name="again_password">
                            <div class="error">&nbsp;
                                @if($errors->has('again_password'))
                                    {{$errors->first('again_password')}}
                                @endif
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-refresh"></i>Reset Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {!! Html::script('js/form/password.js') !!}
@endsection