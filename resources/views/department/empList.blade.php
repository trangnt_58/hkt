<table class="table table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Job</th>
			<th>Email</th>
			<th>Department</th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 1 ?>
			@foreach ($employees as $employee)
			<tr>
				<td>{{count}}</td>
				<td>{{$employee->emp_name}}</td>
				<td>{{$employee->emp_job}}</td>
				<td>{{$employee->emp_email}}</td>
				<td>{{$employee->dep_name}}</td>
			</tr>
			<?php $count++ ?>
			@endforeach
	</tbody>
</table>