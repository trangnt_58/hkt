<div class="modal-dialog depart-form">
	<div class="modal-content">
	<div class="addDep">
	@if ( count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
       		<h3 class="panel-title">Edit Department</h3>
    	</div>
		<div class="modal-body">
			<form id="formDep" method="POST" action="{{url('/department/'.$depart->dep_id.'/update')}}" >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Department Name</label>
					<input type="text" required class="form-control" name="dep_name">
					<div class="error"></div>
				</div>
				<div class="form-group">
					<label>Phone</label>
					<input type="text" required class="form-control" name="dep_phone">
					<div class="error"></div>
				</div>
				<div class="form-group">
					<label>Address</label>
					<input type="text" required class="form-control" name="dep_address">
					<div class="error"></div>
				</div>
				<div class="form-group">
					<label>Manager</label>
					<select class="form-control" id="empList" name="mng_id"></select>
					<script>
					$(document).ready(function() {
						var id = '<?php echo $depart->dep_id;?>';
						$.get('{{url('/department/'.$depart->dep_id.'/getDep') }}',function(data){
							dep = data;
							$('.editDepart-'+id+' input[name="dep_name"]').val(dep.dep_name);
							$('.editDepart-'+id+' input[name="dep_phone"] ').val(dep.dep_phone);
							$('.editDepart-'+id+' input[name="dep_address"] ').val(dep.dep_address);
							var valueSelect =  dep.mng_id;

							$.get('{{ url('/employees/getSelect') }}', function(data ){
								var empList = data;
								var sel = $('.editDepart-'+id+' #empList');
								for(var i = 0; i < empList.length; i++) {
									var opt = document.createElement('option');
									opt.innerHTML = empList[i].emp_name;
									opt.value = empList[i].emp_id;
									sel.append(opt);
								}
								$('.editDepart-'+id+' select[name="mng_id"] ').val(valueSelect);
							});
						});
					});
					</script>
				</div>
					<div class="btnForm">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
{!!Html::script('js/department.js')!!}
