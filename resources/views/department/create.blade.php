<div class="modal-dialog depart-form">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="addDepContent">
            @if ( count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="panel-title">Add New Department</h3>
            </div>
            <div class="modal-body">
                        <form id="formDep" method="POST" action="{{ url('/department/store')}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label>Department Name</label>
                                        <input type="text" required class="form-control" name="dep_name">
                                        <div class="error"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" name="dep_phone">
                                        <div class="error"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" name="dep_address">
                                        <div class="error"></div>
                                    </div>

                                    <div id="addNewDepart" class="form-group">
                                        <label>Manager</label>
                                        <select class="form-control" id="empList" name="mng_id"></select>
                                        <script>
                                            $(document).ready(function(){
                                                $.get('{{ url('/employees/getSelect') }}', function(data ){
                                                    var empList = data;
                                                    var sel = $('#addNewDepart #empList');
                                                    for(var i = 0; i < empList.length; i++) {
                                                        var opt = document.createElement('option');
                                                        opt.innerHTML = empList[i].emp_name;
                                                        opt.value = empList[i].emp_id;
                                                        sel.append(opt);
                                                    }
                                                });
                                            });
                                        </script>
                                        <div class="error"></div>
                                    </div>
                                    <div class="btnForm">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>
                        </form>
                   </div>
                {!! Html::script('js/department.js') !!}
            </div>
    </div>
</div>
