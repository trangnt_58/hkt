@extends('layouts.app')
@section('content')
	<div class="detail-depart profileType">
	<div class="row profileDep main-profile">
		<div class="col-md-2">
			<img  class="img-circle imgDepart" src="{{asset('image/icon/depart1.png')}}"/>
		</div>
		<div class="col-md-8">
			<h3>{{$depart->dep_name}}</h3>
			<p>
				<i class="fa fa-phone" aria-hidden="true"></i>
				 {{$depart->dep_phone}}
			</p>
			<p>
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				{{$depart->dep_address}}
			</p>
		</div>
		@if (Auth::check())
		<div class="col-md-2">
			<div>
				<button class="btn btn-default" data-toggle="modal" data-target=".editDepart-{{$depart->dep_id}}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;
					Edit Info
				</button>
			</div>
			<div>
				<button class="btn btn-danger" data-toggle="modal" data-target=".deleteDepart-{{$depart->dep_id}}">
					<i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;
					Delete
				</button>
			</div>
		</div>
		@endif
	</div>
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#info"> <i class="fa fa-info-circle" aria-hidden="true"></i>
					&nbsp;INFO</a></li>
			<li><a data-toggle="tab" href="#listEmp"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp; LIST EMPLOYEES</a></li>
		</ul>

		<div class="tab-content">
			<div id="info" class="tab-pane fade in active">
				<div class="content">
					<div class="row">
						<div class="col-md-2  col-sm-2 col-xs-5">
							<i class="fa fa-user" aria-hidden="true"></i>
							Manage:
						</div>
						<div class="col-md-10 col-sm-10 col-xs-7">
							<a href="{{ url('/employees/profile/'.$depart->emp_id)}}">{{$depart->emp_name}}</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2  col-sm-2 col-xs-5">
							<i class="fa fa-users" aria-hidden="true"></i>
							Total Employees:
						</div>
						<div class="col-md-10 col-sm-10 col-xs-7">
							<?php echo count($employees); ?>
						</div>
					</div>

				</div>
			</div>
			<div id="listEmp" class="tab-pane fade">
				<button class="btn btn-default viewEmp">List Employee&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i>
				</button>
				<div class="gridDisplay gridEmp">
					@foreach($employees as $employee)
						<div class="itemGrid col-md-4">
							<div class="item row">
								<div class="col-md-3 col-xs-3 col-sm-3">
									@if ($employee->emp_photo!=null)
										<img  class="avatar img-circle" src="{{asset($employee->emp_photo)}}"/>
									@else
										<?php $url=asset('/assets/img/cat.jpg')?>
										<img class="avatar img-circle" src="{{$url}}"/>
									@endif
								</div>
								<div class="col-md-9 col-xs-6 col-sm-6">
									<h4><a href="{{ url('/employees/profile/'.$employee->emp_id)}}">{{$employee->emp_name}}</a></h4>
									<p><i class="fa fa-lightbulb-o" aria-hidden="true"></i>{{$employee->emp_job}}</p>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>


	</div>
	<!--             Modal Edit Depart-->
	<div class="modal fade editDepart-{{$depart->dep_id}}" role="dialog">
				@include('department.edit')
	</div>
	<!--             Modal Delete Depart-->
	<div class="modal fade deleteDepart-{{$depart->dep_id}}" role="dialog" >
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss = "modal">&times;</button>
					<h4 class="modal-title">Confirm Delete</h4>
				</div>
				<div class="modal-body">
					<p>Do you want to delete {{$depart->dep_name}}</p>
				</div>
				<div class="modal-footer">
					<a href="{{ url ('/department/'.$depart->dep_id.'/delete')}}">
						<button class="btn btn-danger"><i class="fa fa-trash-o">&nbsp;Yes</i></button>
					</a>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('.viewEmp').click(function(){
				$('.gridEmp').toggle();
			})
		});
	</script>
@endsection