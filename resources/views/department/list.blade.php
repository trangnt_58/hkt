@extends('layouts.app')
@section('content')
    <div class="department_list">
        <div class="row titlePage">
            <div class="col-md-6">
                <h1 ><i class="fa fa-home"></i> &nbsp;{{trans('menu.department')}}&nbsp;
                    <span class="badge"><?php echo count($department)?></span>
                </h1>
            </div>
            @if (Auth::check())
            <div class="col-md-6">
                <button class="btn btn-primary addNew" data-toggle="modal" data-target="#addDepart"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add New Department</button>
            </div>
            @endif
            <!-- Add Depart -->
            <div class="modal fade" id="addDepart" role="dialog">
                @include('department.create')
            </div>
        </div>
        <hr>
        <div class="content-page">
            <div id ="countEmp">&nbsp;</div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><span>&nbsp;</span>No.</th>
                    <th ><span>&nbsp;</span>Name</th>
                    <th class="canHide"><span>&nbsp;</span>Phone</th>
                    <th class="canHide"><span>&nbsp;</span>Manager</th>
                    @if (Auth::check())<th>Action</th>@endif
                </tr>
                </thead>
                <tbody>
                <?php $count = 1; ?>
                @foreach ($department as $depart)
                    <tr>
                        <td>{{$count}}</td>
                        <td><a href="{{ url('/department/dep/'.$depart->dep_id)}}">{{$depart->dep_name}}</a></td>
                        <td class="canHide">{{$depart->dep_phone}}</td>
                        <td class="canHide"><a href="{{ url('/employees/profile/'.$depart->mng_id)}}">{{$depart->emp_name}}</a></td>
                        @if(Auth::check())
                        <td class="action">
                            <i class="fa fa-pencil-square-o" data-toggle="modal" data-target=".editDepart-{{$depart->dep_id}}"></i> &nbsp;
                            <i class="fa fa-trash-o" data-toggle="modal" data-target=".myModal-{{$depart->dep_id}}"></i>
                        </td>
                            <!--             Modal Delete Depart-->
                            <div class="modal fade myModal-{{$depart->dep_id}}" role="dialog" >
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss = "modal">&times;</button>
                                        <h4 class="modal-title">Confirm Delete</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you want to delete {{$depart->dep_name}}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ url ('/department/'.$depart->dep_id.'/delete')}}">
                                        <button class="btn btn-danger"><i class="fa fa-trash-o">&nbsp;Yes</i></button>
                                        </a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </tr>
                    <?php $count++; ?>
                    <!--   Edit Depart-->
                    <div class="modal fade editDepart-{{$depart->dep_id}}" role="dialog">
                        @include('department.edit')
                    </div>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection