<div class="addMember">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="panel-title">Add Member</h3>
    </div>
    <div class="modal-body">
        <form  method="POST" action="{{ url('/emppro'.'/'.$pro->pro_id)}}" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="text" id="addMemberInput" required class="form-control" name="pro_description" placeholder="Enter Email"/>
            <input type="text" class="hidden" id="empIdValue" name="emp_id"/>
            <button type="submit" class="btn btn-default addMemberBtn"> Add Member</button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#addMemberInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '{{ url('/employees/getEmailJSON') }}' + '/'+ request.term,
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.emp_email,
                                id: item.emp_id,
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $("#addMemberInput").val(ui.item.label); // display the selected text
                $("#empIdValue").val(ui.item.id); // save selected id to hidden input
            },
            minLength: 1
        });
    });
</script>