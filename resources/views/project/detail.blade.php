@extends('layouts.app')
@section('content')
    <div class="project_detail">
        <div class="row">
            <h1 class="col-md-6"><i class="fa fa-bullseye" aria-hidden="true"></i>&nbsp;{{$pro->pro_name}}</h1>
            @if(Auth::check())
                <button class="btn btn-danger" data-toggle="modal" data-target=".deletePro-{{$pro->pro_id}}"><i class="fa fa-trash-o" aria-hidden="true">&nbsp; Delete this project</i>
                </button>
            @endif
        </div>

        <hr>
        <p>Description: {{$pro->pro_description}}</p>
       <div class="emp_list">
           <p>Member: </p>
           <div class="list">
               <div class="list-item">
               @foreach ($employees as $emp)
                   <a href="{{ url('/employees/profile/'.$emp->emp_id)}}">
                   @if ($emp->emp_photo!=null)
                       <img data-toggle="tooltip" data-placement="top" title="{{$emp->emp_name}}" width="40px" height="40px" class="img-circle" src="{{asset($emp->emp_photo)}}"/>
                   @else
                       <img  width="40px" height="40px" class="img-circle" src="{{asset('/assets/img/cat.jpg')}}"/>
                   @endif
                       </a>
               @endforeach
               </div>
               @if(Auth::check())
                   <div class="addNewMem list-item">
                       <i class="fa fa-user-plus" id="addMem" aria-hidden="true"></i>
                       <div class="addMember">
                           <form  method="POST" action="{{ url('/emppro'.'/'.$pro->pro_id)}}" >
                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                               <input type="text" id="addMemberInput" required class="form-control" name="pro_description" placeholder="&#xf067  Enter Email Member"/>
                               <input type="text" class="hidden" id="empIdValue" name="emp_id"/>
                               <button type="submit" class="btn btn-default addMemberBtn"> Add Member</button>
                           </form>

                       </div>
                   </div>
               @endif
           </div>
       </div>

        {{--Tab menu in project detail--}}
        {{--<div class="tab-menu">--}}
            {{--<ul class="nav nav-tabs">--}}
                {{--<li class="active"><a data-toggle="tab" href="#task"> <i class="fa fa-tasks" aria-hidden="true"></i>--}}
                        {{--TASK</a></li>--}}
                {{--<li><a data-toggle="tab" href="#manage"><i class="fa fa-users" aria-hidden="true"></i>MEMBER</a></li>--}}
                {{--<li><a data-toggle="tab" href="#doc"><i class="fa fa-file-text-o" aria-hidden="true"></i>--}}
                        {{--DOCUMENT--}}
                        {{--</i>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
            {{--<div class="tab-content">--}}
                {{--<div id="task" class="tab-pane fade in active">--}}
                    {{--<h3>TASK</h3>--}}
                    {{--<div class="content">--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div id="manage" class="tab-pane fade">--}}
                    {{--Manage--}}
                {{--</div>--}}
                {{--<div id="doc" class="tab-pane fade">--}}
                    {{--No have document--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
        {{--End Tab menu in project detail--}}


    <script>
        $(document).ready(function(){
            //$('.addMember').hide();
            $('[data-toggle="tooltip"]').tooltip();
            $('#addMem').click(function(){
                $('.addMember').toggle();
            });
            $("#addMemberInput").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '{{ url('/employees/getEmailJSON') }}' + '/'+ request.term,
                        dataType: "json",
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.emp_email,
                                    id: item.emp_id,
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    $("#addMemberInput").val(ui.item.label); // display the selected text
                    $("#empIdValue").val(ui.item.id); // save selected id to hidden input
                },
                minLength: 1
            });
            $( "#addMemberInput" ).autocomplete( "option", "appendTo", ".addMember form" );


        });
    </script>
        {{--Delete Project--}}
        <div class="modal fade deletePro-{{$pro->pro_id}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirm Delete</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete {{$pro->pro_name}}?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('/project/'.$pro->pro_id.'/delete')}}">
                            <button class="btn btn-danger"><i class="fa fa-trash-o">&nbsp;Delete</i></button>
                        </a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection