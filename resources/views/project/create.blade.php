<div class="addProject">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="panel-title">Add New Project</h3>
    </div>
    <div class="modal-body">
        <form  method="POST" action="{{ url('/project')}}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Project Name: </label>
                        <input type="text" required class="form-control" name="pro_name">
                        <div class="error"></div>
                    </div>
                    <div class="form-group">
                        <label>Description </label>
                        <textarea type="text" rows="2" required class="form-control" name="pro_description"></textarea>
                        <div class="error"></div>
                    </div>

                    <div class="btnForm">
                        <button type="submit" formnovalidate class="btn">Submit</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>


        </form>

    </div>
</div>



