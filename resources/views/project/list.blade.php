@extends('layouts.app')
@section('content')
    <div class="project_list">
        <div class="row titlePage">
            <div class="col-md-6">
                <h1><i class="fa fa-bullseye" aria-hidden="true"></i>&nbsp;{{trans('menu.project')}}</h1>
            </div>
            @if(Auth::check())
                <div class="col-md-6 addProBtn">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#addPro"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add new Project</button>
                </div>
            @endif
            <!-- Modal Employee -->
            <div class="modal fade" id="addPro" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        @include('project.create')
                    </div>

                </div>
            </div>

        </div>

        <hr>

        @foreach ($project as $pro)
            <a href="{{url('project/'.$pro->pro_id)}}">
            <div class="col-md-4 itemPro">
                <div class="row item">
                    <div class="col-md-3">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-9">
                        <p><h4 class="namePro">{{$pro->pro_name}}</h4></p>
                        <p>{{$pro->name}}</p>
                        {{--<p>trang2uet</p>--}}
                    </div>

                </div>

            </div>
            </a>
        @endforeach
    </div>
@endsection