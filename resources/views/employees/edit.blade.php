<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="editEmp addEmp">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="panel-title">Edit Employee</h3>
        </div>
        <div class="modal-body">
            <form  method="POST" action="{{ url('/employees/'.$employee->emp_id.'/update')}}" enctype="multipart/form-data">                    <div class="row">
                            <div class="col-md-4 avatar">
                                @if ($employee->emp_photo!=null)
                                    <img class="img-circle" src="{{asset($employee->emp_photo)}}"/>
                                @else
                                    <img class="img-circle" src="{{asset('/assets/img/cat.jpg')}}"/>
                                @endif
                                <input type="file" id="fileInput" name="emp_photo" class="hidden"/>
                                <button class="btn" type="button">Choose image</button>
                            </div>

                            <div class="col-md-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label>Employee Name </label>
                                    <input type="text" required class="form-control" name="emp_name">
                                    <div class="error"></div>
                                </div>
                                <div class="form-group">
                                    <label>Job title </label>
                                    <input type="text" required class="form-control" name="emp_job">
                                    <div class="error"></div>
                                </div>

                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" required class="form-control" name="emp_phone">
                                    <div class="error"></div>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" required class="form-control" name="emp_email">
                                    <div class="error"></div>
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control" id="depList" name="dep_id"></select>
                                    <script>
                                        $(document).ready(function(){
                                            var id = '<?php echo $employee->emp_id ?>';

                                            $.get('{{ url('/employees/'.$employee->emp_id.'/getEmp') }}', function(data ) {
                                                emp=data;
                                                $('.editEmp-'+id+' input[name="emp_name"]').val(emp.emp_name);
                                                $('.editEmp-'+id+' input[name="emp_job"]').val(emp.emp_job);
                                                $('.editEmp-'+id+' input[name="emp_phone"]').val(emp.emp_phone);
                                                $('.editEmp-'+id+' input[name="emp_email"]').val(emp.emp_email);
                                                $('.editEmp-'+id+' select[name="dep_id"]').val(emp.dep_id);
                                                $('.editEmp-'+id+' input[name="emp_address"]').val(emp.emp_address);
                                                var valueSelect = emp.dep_id;
                                                $.get('{{ url('/department/getSelect') }}', function(data ){
                                                    var depList = data;
                                                    var sel = $('.editEmp-'+id+ ' #depList');
                                                    for(var i = 0; i < depList.length; i++) {
                                                        var opt = document.createElement('option');
                                                        opt.innerHTML = depList[i].dep_name;
                                                        opt.value = depList[i].dep_id;
                                                        sel.append(opt);
                                                    }
                                                    $('.editEmp-'+id+' select[name="dep_id"]').val(valueSelect);
                                                });
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="emp_address">
                                </div>
                                <div class="btnForm">
                                    <button type="submit" formnovalidate class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>

                        </div>

            </form>

            </div>
        </div>
       </div>
    </div>


