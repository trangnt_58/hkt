<table class="listDisplay table table-striped" id="empTable">
    <thead>
    <tr>
        <th>No.</th>
        <th onclick="sortcol(this);"><span>&nbsp;</span>Name</th>
        <th class="canHide" onclick="sortcol(this);"><span>&nbsp;</span>Job</th>
        <th class="canHide" onclick="sortcol(this);"><span>&nbsp;</span>Email</th>
        <th class="canHide" onclick="sortcol(this);"><span>&nbsp;</span>Department</th>
        @if(Auth::check())<th>Action</th>@endif
    </tr>
    </thead>
    <tbody>
    <?php $count = 1; ?>
    @foreach ($employees as $employee)
        <tr>
            <td>{{$count+($employees->currentPage()-1)* $employees->perPage()}}</td>
            <td>
                @if ($employee->emp_photo!=null)
                    <img  class="img-circle avatar" src="{{asset($employee->emp_photo)}}"/>
                @else
                    <img class="img-circle avatar" src="{{asset('/assets/img/cat.jpg')}}"/>
                @endif
                <a href="{{ url('/employees/profile/'.$employee->emp_id)}}">{{$employee->emp_name}}</a></td>
            <td class="canHide">{{$employee->emp_job}}</td>
            <td class="canHide">{{$employee->emp_email}}</td>
            <td class="canHide">{{$employee->dep_name}}</td>
            @if(Auth::check())
                <td class="action">
                    <i class="fa fa-pencil-square-o" data-toggle="modal" data-target=".editEmp-{{$employee->emp_id}}"></i>
                    &nbsp;
                    <i class="fa fa-trash-o" data-toggle="modal" data-target=".myModal-{{$employee->emp_id}}"></i>
                </td>
                <div class="modal fade myModal-{{$employee->emp_id}}" role="dialog">
                   @include('employees.delete');
                </div>
                <div class="modal fade editEmp-{{$employee->emp_id}}" role="dialog">
                    @include('employees.edit');
                </div>
            @endif
        </tr>
        <?php $count++; ?>
    @endforeach
    </tbody>
</table>
<div class="gridDisplay">
    @foreach ($employees as $employee)
        <div class="itemGrid col-md-6">
            <div class="item row">
                <div class="col-md-3 col-xs-3 col-sm-3">
                    @if ($employee->emp_photo!=null)
                        <img  class="avatar" src="{{asset($employee->emp_photo)}}"/>
                    @else
                        <?php $url=asset('/assets/img/cat.jpg')?>
                        <img class="avatar" src="{{$url}}"/>
                    @endif
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6">
                    <h4><a href="{{ url('/employees/profile/'.$employee->emp_id)}}">{{$employee->emp_name}}</a></h4>
                    <p><i class="fa fa-envelope-o" aria-hidden="true"></i>{{$employee->emp_email}}</p>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i>{{$employee->dep_name}}</p>
                </div>
                @if(Auth::check())
                <div class="col-md-3 col-xs-3 col-sm-3 action">
                    <i class="fa fa-pencil-square-o" data-toggle="modal" data-target=".editEmp-{{$employee->emp_id}}"></i> &nbsp;
                    <i class="fa fa-trash-o" data-toggle="modal" data-target=".myModal-{{$employee->emp_id}}"></i>
                </div>
                @endif
            </div>
        </div>
    @endforeach
</div>
{!! $employees->links() !!}
{!! Html::script('assets/js/employeesList.js') !!}

