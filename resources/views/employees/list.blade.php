@extends('layouts.app')
@section('content')
    <div class="employees_list">
        <div class="row titlePage">
            <div class="col-md-6">
                <h1><i class="fa fa-users"></i>&nbsp;{{trans('menu.employees')}} &nbsp;<span class="badge">{{$countEmp}}</span></h1>
            </div>
            @if (Auth::check())
            <div class="col-md-6">
                <button class="btn btn-primary addNew" data-toggle="modal" data-target="#addEmployee"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add New Employee</button>
            </div>
            @endif
        </div>
        <hr>
        <div class="content-page">
            {{--@if(Session::has('action_success'))--}}
                {{--<div class="alert alert-success action-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('action_success') !!}</em></div>--}}
            {{--@endif--}}
            <div class="row toolbar">
                <div class="col-xs-12 col-sm-6 col-md-5">
                    <div class="right-inner-addon">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <input id="searchValue" type="search" class="form-control" placeholder="Search" />
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 searchType">
                    <span> Search by</span>
                    <select id="searchSel">
                        <option value="All">All</option>
                        <option value="Name">Name</option>
                        <option value="=Job">Job</option>
                        <option value="Email">Email</option>
                        <option value="Department">Department</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 wayDisplay">
                    <button class="btn btn-default btnList"><i class="fa fa-th-list" aria-hidden="true"></i></button>
                    <button class="btn btn-default btnGrid"> <i class="fa fa-th-large" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="ajaxLoader">
                @include('employees.pagination')
            </div>
        </div>
        <!-- Modal Employee -->
        <div class="modal fade" id="addEmployee" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                   @include('employees.create')
                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.action-success').fadeOut(1000);
            function searchAction() {
                var key = $('#searchValue').val().trim();
                var searchType =  $('#searchSel :selected').text().toLowerCase();
                if(key != '') {
                    $.get('{{url('/employees/search')}}' + '/' + key + '/' + searchType, function (data) {
                        $('.ajaxLoader').html(data);
                    });
                }
            }
            $("#searchValue").keyup(function() {
                searchAction();
            });
            $('#searchSel').change(function () {
                searchAction();
            });

            function updateCountEmp(){
                var count = 0;
                $('#empTable tbody tr').each(function(){
                    if ( $(this).css('display') != 'none'){
                        count++;
                    }
                });
                $('#countEmp').text(count +' result');
            }


        });

        $(document).on('click','.pagination a',function(e){
            e.preventDefault();
            var type;
            if($(this).attr('href').indexOf('search') >= 0){
                type = 'search';
            }else {
                type = 'index';
            }
            var page = $(this).attr('href').split('page=')[1];
            getEmployees(page,type);
        });
        function getEmployees(page,type) {
            if(type == 'index') {
                $.ajax({
                    url: '/ajax/employees?page=' + page
                }).done(function (data) {
                    $('.ajaxLoader').html(data);
                    location.hash = page;
                });
            }
            if(type == 'search') {
                var key = $('#searchValue').val();
                var searchType =  $('#searchSel :selected').text().toLowerCase();
                $.ajax({
                    url: '/employees/search/' + key + '/' + searchType + '?page=' + page
                }).done(function (data) {
                    $('.ajaxLoader').html(data);
                    location.hash = page;
                });
            }
        }
    </script>
@endsection