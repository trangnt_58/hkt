<div class="task-list">
    @foreach($task as $itask)
        <div class="task-item row" id="task{{$itask->task_id}}">
            <meta name="csrf_token" content="{{ csrf_token() }}" />
            <div class="col-md-9 col-sm-9 nameTask">
                {{--@if ($itask->task_status == 1)--}}
                    {{--<input type="checkbox" checked>{{$itask->task_name}}--}}
                {{--@else--}}
                    {{$itask->task_name}}
                {{--@endif--}}
            </div>

            <div class="col-md-3 col-sm-3 action">
                <span class="delete-task">
                    <i class="fa fa-trash"></i>
                </span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                //delete task and remove it from list
                $('#task'+ '{{$itask->task_id}}'+' .delete-task').click(function(){
                    var task_id = '<?php echo $itask->task_id ?>';
                    $.ajax({
                        type: "POST",
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');
                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        url: '{{ url('/task') }}'+'/'+task_id+'/delete',
                        success: function (data) {
                            $("#task" + task_id).hide();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                });


            });
        </script>
    @endforeach
</div>
<style>
    .task-list .action {
        display:none;
    }
</style>
<script>
    $(document).ready(function(){
        $('.task-item').hover(function(){
            $(this).find('.action').show();
            },function(){
            $(this).find('.action').hide();
        });
        //add task and append to list
        $('#add-task').click(function(){
            var emp_id = '{{$employee->emp_id}}';
            console.log(emp_id);
            var formData = {
                task_name: $('input[name="task_name"]').val()
            };
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                url: '{{ url('employees/profile/')}}'+'/'+emp_id+'/createTask',
                data: formData,
                success: function (data) {
                    var status = data.task_status;
                    if(status == 0 ) {
                        var task = '<div class="task-item row" id="task'+ data.task_id +'"><div class="col-md-9 col-sm-9 nameTask">'
                                + data.task_name + '</div>'
                                + '<div class="col-md-3 col-sm-3 action">'
                                +'<span class="delete-task">'
                                +' <i class="fa fa-trash"></i></span> </div></div>';
                        $('.task-list').append(task);
                        $('.task-item').hover(function(){
                            $(this).find('.action').show();
                        },function(){
                            $(this).find('.action').hide();
                        });
                    }
                    $('input[name="task_name"]').val('');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });


    });

</script>