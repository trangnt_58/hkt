<div class="addEmp addNew">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="panel-title">Add New Employee</h3>
    </div>
    <div class="modal-body">
        <form id="formEmp" method="POST" action="{{ url('/employees/store')}}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-4 avatar">
                    <img src="{{asset('assets/img/cat.jpg')}}">
                    <input type='file' id="imgAvatar" name="emp_photo" />
                    <button class="btn" type="button">Choose image</button>
                </div>
                <div class="col-md-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Employee Name </label>
                            <input type="text"  class="form-control" name="emp_name">
                        </div>
                        <div class="form-group">
                            <label>Job title </label>
                            <input type="text" class="form-control" name="emp_job">
                        </div>
                        <div class="form-group">
                                <label>Phone</label>
                                <input type="text"  class="form-control" name="emp_phone">
                        </div>
                        <div class="form-group">
                                <label>Email</label>
                                <input type="text"  class="form-control" name="emp_email">
                                <div class="error error_email"></div>
                        </div>
                        <div class="form-group">
                                <label>Department</label>
                                <select class="form-control" id="depList" name="dep_id"></select>
                                <script>
                                    $(document).ready(function(){

                                        $('.addEmp input[name="emp_email"]').blur(function(){
                                            var isValid = true;
                                            var emailEnter = $(this).val();
                                            $.get('{{ url('/employees/getEmpJSON') }}', function(data ){
                                                for( var i = 0; i < data.length; i++) {
                                                    if (emailEnter == data[i].emp_email) {
                                                        $('.addEmp .error_email').text('Email already exists');
                                                        isValid = false;
                                                        break;
                                                    }
                                                }
                                                if(!isValid) {
                                                    $('#createEmpBtn').prop('disabled', true);
                                                }else{
                                                    $('#createEmpBtn').prop('disabled', false);
                                                }
                                            });
                                        });
                                        $('.addEmp input[name="emp_email"]').keyup(function(){
                                            $('.addEmp .error_email').text('');
                                        });
                                        $.get('{{ url('/department/getSelect') }}', function(data ){
                                            var depList = data;
                                            var sel = $('#addEmployee #depList');
                                            for(var i = 0; i < depList.length; i++) {
                                                var opt = document.createElement('option');
                                                opt.innerHTML = depList[i].dep_name;
                                                opt.value = depList[i].dep_id;
                                                sel.append(opt);
                                            }

                                        });
                                    });
                                </script>
                        </div>
                        <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="emp_address">
                        </div>
                        <div class="cv">
                            <label>Upload CV</label>
                            <input class="inputCV" type="file"  class="form-control" name="emp_cv">
                        </div>
                        <div class="btnForm">
                                <button type="submit" id="createEmpBtn" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                </div>
            </div>
        </form>
    </div>
</div>
{!! Html::script('js/form/employees.js') !!}
{!! Html::script('js/employeesCreate.js') !!}


