@extends('layouts.app')
@section('content')
    <div class="profile-emp profileType">
        <div class="row profileEmp main-profile">
            <div class="col-md-2 avatar">
                @if ($employee->emp_photo!=null)
                    <img  class="img-circle" src="{{asset($employee->emp_photo)}}"/>
                @else
                    <img  class="img-circle" src="{{asset('/assets/img/cat.jpg')}}"/>
                @endif
            </div>
            <div class="col-md-8">
                <h3>{{$employee->emp_name}}</h3>
                <p>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    {{$employee->emp_email}}
                </p>
                <p>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    {{$employee->dep_name}}
                </p>
            </div>
            @if (Auth::check())
            <div class="col-md-2">
                <div>
                    <button class="btn btn-default" data-toggle="modal" data-target=".editEmp-{{$employee->emp_id}}">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;
                        Edit Info
                    </button>
                </div>
                <div>
                    <button class="btn btn-danger" data-toggle="modal" data-target=".deleteEmp-{{$employee->emp_id}}">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;
                        Delete
                    </button>
                </div>
            </div>
            @endif
        </div>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#info"> <i class="fa fa-info-circle" aria-hidden="true"></i>
                    INFO</a></li>
            <li><a data-toggle="tab" href="#task"><i class="fa fa-tasks" aria-hidden="true"></i> TASK</a></li>
            <li><a data-toggle="tab" href="#menu2">
                    <i class="fa fa-bullseye" aria-hidden="true">
                        PROJECT
                    </i>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="info" class="tab-pane fade in active">
                <h3>Profile</h3>
                <div class="content">
                    <div class="row">
                        <div class="col-md-2  col-sm-2 col-xs-5">
                            <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                           Job:
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-7">
                           {{$employee->emp_job}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2  col-sm-2 col-xs-5">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            Phone:
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-7">
                            {{$employee->emp_phone}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2  col-sm-2 col-xs-5">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Address:
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-7">
                            {{$employee->emp_address}}
                        </div>
                    </div>
                    <hr>
                    <h3>CV</h3>
                    @if(Auth::check())
                        @if($employee->emp_cv != NULL)
                        <a href="{{url('/employees/'.$employee->emp_id.'/downloadCV')}}" target="_blank">Download CV</a>
                        @else
                        No CV
                        @endif
                    @else
                        <a href="{{url('/login')}}">Login to see detail</a>
                    @endif

                </div>
            </div>
            <div id="task" class="tab-pane fade">
                <div class="col-md-6">
                    <h3>List Task</h3>
                    @if (Auth::check())
                        @include('employees.task')
                        <div class="row add-task">
                            <div class="col-md-9">
                                <input type="text" id="inputTask" placeholder="Add a task" name="task_name"/>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-default" id="add-task">Add Task</button>
                            </div>
                        </div>
                    @else
                        <a href="{{url('/login')}}">Login to see detail</a>
                    @endif
                </div>
            </div>
            <div id="menu2" class="tab-pane fade">
                <div class="list_pro">
                </div>
                    <script>
                        $(document).ready(function(){
                            var id = '<?php echo $employee->emp_id ?>';
                            $.get('{{ url('/employees') }}'+'/' + id + '/getPro', function(data ){
                                $('.list_pro').html(data);
                            });
                        });

                    </script>
            </div>
        </div>
    </div>
    <div class="modal fade deleteEmp-{{$employee->emp_id}}" role="dialog">
        @include('employees.delete');
    </div>
    <div class="modal fade editEmp-{{$employee->emp_id}}" role="dialog">
        @include('employees.edit');
    </div>
    <div class="modal fade" id="addTask" role="dialog">
        @include('task.create');
    </div>
@endsection