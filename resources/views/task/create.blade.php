<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Task</h4>
        </div>
        <div class="modal-body">
            <form  method="POST" action="{{ url('/employees/profile/'.$employee->emp_id.'/createTask')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Task Name </label>
                            <input type="text" required class="form-control" name="task_name">
                            <div class="error"></div>
                        </div>
                        <div class="form-group">
                            <label>Description </label>
                            <input type="text" class="form-control" name="task_description">
                            <div class="error"></div>
                        </div>
                        <div class="form-group">
                            <label>Deadline</label>
                            <input type="date" class="form-control" name="task_dealine">
                            <div class="error"></div>
                        </div>

                        <div class="btnForm">
                            <button type="submit" formnovalidate class="btn">Submit</button>
                            <button type="button" class="btn"  data-dismiss="modal">Cancel</button>
                        </div>

            </form>
        </div>
    </div>
</div>