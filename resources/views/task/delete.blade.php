<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Confirm Delete</h4>
        </div>
        <div class="modal-body">
            <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
            <a href="{{ url('')}}">
                <button class="btn btn-danger"><i class="fa fa-trash-o">&nbsp;Delete</i></button>
            </a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>