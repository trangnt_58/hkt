@extends('layouts.app')
@section('content')
    <div class="dashboard">
        <div class="titlePage">
            <h1><i class="fa fa-tachometer"></i>&nbsp;{{trans('menu.dashboard')}}</h1>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon blue"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <div class="mini-stat-info">
                        <span class="count">{{$countAdmin}}</span>
                        Admin
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon orange"><i class="fa fa-users" aria-hidden="true"></i></span>
                    <div class="mini-stat-info">
                        <span class="count">{{$countEmployees}}</span>
                        Employees
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon green"><i class="fa fa-home" aria-hidden="true"></i></span>
                    <div class="mini-stat-info">
                        <span class="count">{{$countDepart}}</span>
                        Department
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 800,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>

@endsection
