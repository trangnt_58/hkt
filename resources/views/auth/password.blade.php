<html>
    <head>
        <title> Reset Password</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1> Kh�i ph?c m?t kh?u</h1>
        <form method="post" action="{{url('/auth/login')}}">
            {!! csrf_field() !!}
            @if (count($errors) > 0 )
                <div>
                    C� l?i x?y ra trong qu� tr�nh kh�i ph?c m?t kh?u:
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                <label for="email">Email: </label>
                <input type="email" name="email" id="email" placeholder="Email">
            </div>
            <div>
                <button type="submit"> G?i link kh�i ph?c m?t kh?u</button>
            </div>
        </form>

   </body>
</html>