<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::controllers([
    'password' => 'Auth\PasswordController',
]);
Route::get('/admin','AdminController@index');
Route::get('/admin/{id}/delete','AdminController@delete');
Route::post('/admin/create','AdminController@store');
Route::get('/dashboard',function(){
    return view("dashboard");
});

/* Admin */
Route::post('/login','AdminController@postLogin');
Route::get('/login','AdminController@getLogin');
Route::get('/admin/password/change','AdminController@getChangePassword');
Route::post('/admin/password/change','AdminController@postChangePassword');
Route::get('/getFormChange','AdminController@getFormChange');
Route::get('/admin/getProfile','AdminController@getProfile');
Route::get('/admin/getAllEmail','AdminController@getAllEmail');

/* Employees */
Route::get('/employees/create', 'EmployeesController@create');
Route::post('/employees/store','EmployeesController@store');
Route::get('/employees','EmployeesController@index');
Route::get('/employees/profile/{id}','EmployeesController@show');
Route::get('/employees/{id}/delete','EmployeesController@delete');
Route::get('/employees/{id}/edit','EmployeesController@edit');
Route::post('/employees/{id}/update','EmployeesController@update');
Route::get('/employees/{id}/getEmp','EmployeesController@getEmp');
Route::get('/ajax/employees','EmployeesController@getAjaxEmp');
Route::get('/getAllEmp','EmployeesController@getAllEmp');
Route::get('/employees/search/{keyword}/{searchType}','EmployeesController@searchEmp');
Route::get('/employees/getSelect','EmployeesController@getSelect');
Route::get('/employees/downloadCV','EmployeesController@getDownloadCV');
Route::get('/employees/getEmpJSON','EmployeesController@getEmpJSON');
Route::get('/employees/getEmailJSON/{email}','EmployeesController@getEmailJSON');
Route::get('/employees/{id}/getPro','EmployeesController@getProject');
Route::get('employees/{id}/downloadCV','EmployeesController@downloadCV');


/*Department */
Route::get('/department','DepartmentController@index');
Route::get('/department/getSelect','DepartmentController@getSelect');
Route::get('/department/create','DepartmentController@create');
Route::post('/department/store', 'DepartmentController@store');
Route::get('/department/dep/{id}', 'DepartmentController@show');
Route::get('/department/{id}/getDep','DepartmentController@getDep');
Route::get('/department/{id}/edit', 'DepartmentController@edit');
Route::post('/department/{id}/update','DepartmentController@update');
Route::get('/department/{id}/delete', 'DepartmentController@delete');

/* Dashboard */
Route::get('/dashboard','DashboardController@index');

/* Project */
Route::resource('project', 'ProjectController');
Route::get('/project/{id}/delete', 'ProjectController@delete');

/* Task */
Route::resource('task', 'TaskController');
Route::post('employees/profile/{id}/createTask','TaskController@storeTask');
Route::post('task/{id}/delete','TaskController@delete');
Route::delete('/tasks/{id}','TaskController@deleteTask');

/*Employees - Tasks - Project */
Route::resource('emppro','EmpProController');
Route::post('/emppro/{pro_id}','EmpProController@store');
Route::get('employees/profile/{id}/getTask','EmpProController@getTask');

/*Test */
Route::get('admin/profile', ['middleware' => 'admin', function () {

}]);

/* Localization */
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

/* About Us */
Route::get('/about_us', function(){
    return view('about_us');
});