<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmployeesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emp_name'  => 'required',
            'emp_job'  => 'required',
            'emp_email'  => 'required',
            'emp_phone' => 'required',
            'emp_address' =>'required',
        ];
    }
}
