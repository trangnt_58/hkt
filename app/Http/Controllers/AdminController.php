<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Http\Requests\LoginRequest;

use Illuminate\Support\MessageBag;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

use Mail;

use DB;

use Auth;

use Hash;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin', ['except' => ['postLogin','getChangePassword','postChangePassword','getLogin','getProfile']]);
	}
    public function index(){
		$users = User::all();
		return view("admin.list")->with("users", $users);
	}

	public function getAllEmail(){
		$users = User::all();
		return $users;
	}
	
	protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

	public function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	public function store(Request $request){
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required'
		]);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		$email = $request->input('email');
		$status = 0;
		$randPassword = AdminController::randomPassword();
		$data = $request->all();
	 	User::create([
            'name' => $data['name'],
            'email' => $data['email'],
			'password' => bcrypt($randPassword),
			'status' => $status
        ]);
		Mail::send('admin.email.defaultPassword', ['name' => $request->input('email'),'pass' => $randPassword], function($message){
			$message->to(Input::get('email'), 'Trang Nguyen')->subject('Welcome to Hakata App!');
		});
		\Session::flash('response_success','Add Admin Success');
		return redirect('/admin');
	}
	function getStatus($email){
		$user = DB::table('users')->where('email',$email)->first();
		if($user->status == 0) return false;
		else return true;
	}

	function postLogin(LoginRequest $request){
		$auth = array(
			'email' => $request->email,
			'password' => $request->password
		);

		if(Auth::attempt($auth)){
			$changedPass = AdminController::getStatus($request->email);
			if($changedPass == true){
				return redirect('/');
			}else {
				return redirect('/admin/password/change');
			}
		}else{
			$errors = new MessageBag(['invalid' => ['Email and/or password invalid.']]);
			return Redirect::back()->withErrors($errors);
		}
	}

	public function getChangePassword(){
		return view('admin.password.change');
	}

	function postChangePassword(Request $request){
		$validator = Validator::make($request->all(), [
			'old_password' => 'required|min:6',
                'password' => 'required|min:6',
                'again_password' => 'required|min:6|same:password'
		]);

		if ($validator->fails()) {
			return redirect('/admin/password/change')
				->withErrors($validator)
				->withInput();

        }else{
            $user = User::find(Auth::user()->id);
            $old_password = Input::get('old_password');
            $password = Input::get('password');
            if(Hash::check($old_password, $user->getAuthPassword())){
                $user->password = Hash::make($password);
				if(Auth::user()->status == 0){
					$user->status = 1;
				}
				$user->save();
				\Session::flash('response_success','Change password successfully');
				return redirect('/dashboard');
				//return redirect()->back();
            }
		}
	}

	function getLogin() {
		return view('admin.login');
	}

	public function delete($id){
		User::find($id)->delete();
		return redirect()->action('AdminController@index');
	}

	public function getProfile() {
		return view('admin.profile');
	}
}
