<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use App\Http\Requests;

use App\Http\Requests\EmployeesRequest;

use App\Employees;

use Intervention\Image\ImageManagerStatic as Image;

use DB;

use Illuminate\Support\Facades\Validator;



class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['index','show','getAjaxEmp','searchEmp','getProject']]);
    }
    /* Return to add select in department */
    public function getSelect(){
        $employees = DB::select('select * from employees');
        return $employees;
    }
    /* Return JSON to add select in department */
    public function getEmpJSON(){
        $employees = DB::select('select employees.emp_id, employees.emp_email from employees');
        return response()->json($employees);
    }
    /* validate email */
    public function getEmailJSON($email){
        $employees = DB::table('employees')
            ->where('employees.emp_email','like','%'.$email.'%')->select('emp_id','emp_email')->get();
        return response()->json($employees);
    }
    /* get all employee */
    public function index(){
        $countEmp = DB::table('employees')->count();
        $employees = DB::table('employees')
            ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
            ->paginate(5);
        return view('employees.list', ['employees' => $employees,'countEmp' =>$countEmp]);
    }
    /* pagination in list employee */
    public function getAjaxEmp(){
        $employees = DB::table('employees')
            ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
            ->paginate(5);
        return view('employees.pagination', ['employees' => $employees])->render();
    }
    /* Search employees function */
    public function searchEmp($keyword, $searchType) {

        // in case of searching all fields
        if($searchType == 'all'){
            $employees = DB::table('employees')
                ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
                ->where('employees.emp_name','like','%'.$keyword.'%')
                ->orWhere('employees.emp_job','like','%'.$keyword.'%')
                ->orWhere('employees.emp_email','like','%'.$keyword.'%')
                ->orWhere('department.dep_name','like','%'.$keyword.'%')
                ->paginate(5);
        }

        /* otherwise */
        else{
            if($searchType == 'department'){
                $employees = DB::table('employees')
                    ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
                    ->where('department.dep_name','like','%'.$keyword.'%')->paginate(5);

            }else{
                $employees = DB::table('employees')
                    ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
                    ->where('employees.emp_'.$searchType,'like','%'.$keyword.'%')->paginate(5);
            }

        }
        return view('employees.pagination', ['employees' => $employees])->render();
    }
    /* show information of employee */
    public function show($emp_id) {
        $employee = DB::table('employees')
            ->leftJoin('department', 'employees.dep_id', '=', 'department.dep_id')
            ->where('employees.emp_id',$emp_id)->first();
        $task = DB::table('task')->where('emp_id',$emp_id)->get();
        return view('employees.profile')->with(['employee' => $employee, 'task' => $task]);
    }
    /*get project information */
    public function getProject ($emp_id) {
        $project = $employees = DB::table('emp_task_pro')
            ->join('project', 'emp_task_pro.pro_id', '=', 'project.pro_id')
            ->where('emp_task_pro.emp_id',$emp_id)->select('project.pro_id','project.pro_name')->get();
        return view('employees.project')->with("project",$project);
    }

    /* return all employees of company */
    public function getAllEmp(){
        $employees = DB::select('SELECT e.emp_name, e.emp_job, e.emp_email, d.dep_name FROM employees e LEFT JOIN department d ON e.dep_id = d.dep_id');
        return $employees;

    }

    /* get employee infor with selecte id */
    public function getEmp($emp_id) {
        $employees = Employees::find($emp_id);
        return $employees;
    }

    /* return employee create form */
    public function create() {
        return view("employees.create");
    }

    /* update data to database */
    public function store(EmployeesRequest $request){
        $data = $request->all();
        $image = Input::file('emp_photo');
        /* Save image by path */
        if ($image != NULL) {
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $path = public_path('image/profilepics/' . $filename);
            Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $filename = 'image/profilepics/' . $filename;
        }
        $cv = Input::file('emp_cv');

        /* save without cv */
        if ($cv != NULL) {
            $fileCV = time().'_'.$cv->getClientOriginalName();;
            $cv->move(public_path('file/cv/'), $fileCV);
            $pathCV = 'file/cv/'.$fileCV;
        }else {
            $pathCV = '';
        }

        /* save without image */
        if ($image != NULL)
        Employees::create([
            'emp_name' => $data['emp_name'],
            'emp_photo' => $filename,
            'emp_job' => $data['emp_job'],
            'emp_email' => $data['emp_email'],
            'emp_phone' => $data['emp_phone'],
            'emp_address' => $data['emp_address'],
            'dep_id' => $data['dep_id'],
            'emp_cv' => $pathCV


        ]);

        /* otherwise */
        else
         Employees::create([
             'emp_name' => $data['emp_name'],
             'emp_job' => $data['emp_job'],
             'emp_email' => $data['emp_email'],
             'emp_phone' => $data['emp_phone'],
             'emp_address' => $data['emp_address'],
             'dep_id' => $data['dep_id'],
              'emp_cv' => $pathCV

         ]);

        /* message return when do success */
        \Session::flash('response_success','Add employee successfully');
        \Session::flash('action_success','Add employee successfully');
        return redirect('/employees');
    }

    /* return edit form */
    public function edit($emp_id){
        $employees = Employees::findOrFail($emp_id);
        return view('employees.edit')->with("employees", $employees);
    }

    /* update validated infor */
    public function update($emp_id , EmployeesRequest $request){
        $employees = Employees::findOrFail($emp_id);
        $image = Input::file('emp_photo');
        $data = $request->all();

        if ($image != NULL){
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('image/profilepics/' . $filename);
            Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $filename  = 'image/profilepics/'. $filename;
            Employees::where('emp_id',$emp_id)->update(array(
                'emp_name' => $data['emp_name'],
                'emp_photo' => $filename,
                'emp_job' => $data['emp_job'],
                'emp_email' => $data['emp_email'],
                'emp_phone' => $data['emp_phone'],
                'emp_address' => $data['emp_address'],
                'dep_id' => $data['dep_id']

            ));
        }

        else
            Employees::where('emp_id',$emp_id)->update(array(
                'emp_name' => $data['emp_name'],
                'emp_job' => $data['emp_job'],
                'emp_email' => $data['emp_email'],
                'emp_phone' => $data['emp_phone'],
                'emp_address' => $data['emp_address'],
                'dep_id' => $data['dep_id']

            ));

        \Session::flash('action_success','Edit employee successfully');
        \Session::flash('response_success','Edit employee successfully');

        return redirect()->back();
    }

    /* delete an employee from database */
    public function delete($emp_id){
        Employees::find($emp_id)->delete();
        \Session::flash('action_success','Delete employee successfully');
        \Session::flash('response_success','Delete employee successfully');

        return redirect('/employees');
    }

    /* download existed CV */
    public function getDownloadCV()
    {
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/download/cv.pdf";
        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file, 'cv.pdf', $headers);
    }
    public function downloadCV($emp_id) {
        $path = DB::table('employees')
            ->where('employees.emp_id',$emp_id)->select('emp_cv')->get();
        $file= public_path().'/'.$path[0]->emp_cv;

        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->download($file, 'cv.pdf', $headers);

    }
}
