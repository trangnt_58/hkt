<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class DashboardController extends Controller
{
    public function index(){
        /*Count in tables */
        $admin = DB::table('users')->count();
        $employees = DB::table('employees')->count();
        $depart = DB::table('department')->count();
        $data = [
            'countAdmin'  => $admin,
            'countEmployees' => $employees,
            'countDepart' => $depart
    ];
        //return view('dashboard')->with("countAdmin",$countAdmin);
        return view('dashboard')->with($data);
    }
}
