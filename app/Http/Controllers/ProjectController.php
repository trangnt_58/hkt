<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Auth;

use App\Project;

use App\Employees;

use App\Http\Requests;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$project = DB::table('project')->distinct()->get();
        $project = DB::table('project')
            ->leftJoin('users','project.pro_own','=','users.id')->get();

        return view('project.list',['project'=> $project]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $pro_own = Auth::user()->id;
        Project::create([
            'pro_name' => $data['pro_name'],
            'pro_own' => $pro_own,
            'pro_description' => $data['pro_description']
        ]);
        //$messages = $validator->error();

        return redirect('/project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pro_id)
    {
        $project = Project::find($pro_id);
        $employees = DB::table('project')
            ->join('emp_task_pro', 'project.pro_id', '=', 'emp_task_pro.pro_id')
            ->join('employees', 'emp_task_pro.emp_id', '=', 'employees.emp_id')
            ->where('project.pro_id','=',$pro_id)->get();
        $pro_own = DB::table('project')
            ->join('users','project.pro_own','=','users.id')->where('project.pro_id','=',$pro_id)->get();
        return view('project.detail')->with(["pro" => $project, "employees" => $employees,"pro_own" => $pro_own]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pro_id)
    {
        Project::find($pro_id)->delete();
        return redirect('/project');
    }
    public function delete($pro_id)
    {
        Project::find($pro_id)->delete();
        return redirect('/project');
    }

    public function addMember($pro_id, $emp_id) {

    }

}
