<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use App\Department;

use App\Http\Requests\DepartmentRequest;


class DepartmentController extends Controller
{

    // return all deparment in database
    public function getSelect(){
        $department = DB::select('select * from department');
        return $department;
    }

    // join 2 table depertments and employees to get manager name
    public function index(){
        $department = DB::table('department')
            ->leftJoin('employees', 'employees.emp_id', '=', 'department.mng_id')->select('department.*','employees.emp_name')->get();
        return view('department.list', ['department' => $department]);
    }

    // show details about a department with manager name
    public function indexDep(){
        $department = DB::select('select * from employee e join department d on e.dep_id = d.dep_id;');
        return view ('department.details',['department'=>$department]);
    }

    // show department information and list employees in this one
    public function show($dep_id){

        // get department infor
        $depart = DB::table('department')
            ->leftjoin('employees','department.mng_id','=','employees.emp_id')
            ->where ('department.dep_id',$dep_id)->select('department.*','employees.emp_name','employees.emp_id')->first();

        // get list employees
        $employees = DB::table('employees')
            ->leftjoin ('department', 'department.dep_id', '=','employees.dep_id')->select('employees.*','department.dep_name')
            ->where('employees.dep_id','=',$dep_id)->get();

        return view('department.details')->with(["depart"=> $depart,"employees" => $employees]);
    }

    // return dep infor
    public function getDep($dep_id){
        $department = Department::find($dep_id);
        return $department;
    }

    // return department create view
    public function create(){
        return view("department.create");
    }

    // create new department
    // information entered in form create
    public function store(DepartmentRequest $request){
        $data = $request->all();
        Department::create([
            'dep_name' => $data['dep_name'],
            'dep_phone' => $data['dep_phone'],
            'dep_address' => $data['dep_address'],
            'mng_id' => $data['mng_id']
        ]);
        \Session::flash('response_success','Add department success');
        return redirect('/department');
    }

    // redirect to department edit form
    public function edit($dep_id){
        $department = Department::findOrFail($dep_id);
        return view('department.edit')->with("department", $department);
    }

    // update new infor to database
    public function update($dep_id, DepartmentRequest $request){
        $department = Department::findOrFail($dep_id);
        $data = $request->all();
        Department::where('dep_id', $dep_id)->update(array(
            'dep_name' => $data['dep_name'],
            'dep_phone' => $data['dep_phone'],
            'dep_address' => $data['dep_address'],
             'mng_id' => $data['mng_id']
        ));
        \Session::flash('response_success','Edit department success');
        return redirect()->back();
    }
    // delete department with selected id
    public function delete($dep_id){
        Department::find($dep_id)->delete();
        \Session::flash('response_success','Delete department success');
        return redirect('/department');
    }
}