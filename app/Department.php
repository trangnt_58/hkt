<?php
/**
 * Created by PhpStorm.
 * User: Trangnt
 * Date: 5/7/2016
 * Time: 11:48 AM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;


class Department extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'dep_name', 'dep_phone', 'dep_address', 'mng_id'
    ];
    protected $table = 'department';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'dep_id';

}