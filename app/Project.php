<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'pro_id', 'pro_name','pro_description','pro_own'
    ];

    protected $table = 'project';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pro_id';
}
