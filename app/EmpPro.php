<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpPro extends Model
{
    protected $fillable = [
        'emp_id', 'task_id', 'pro_id'
    ];

    protected $table = 'emp_task_pro';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
