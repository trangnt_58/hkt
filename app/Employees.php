<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $fillable = [
        'emp_name', 'emp_photo','emp_job','emp_email', 'emp_phone','emp_address','dep_id','emp_cv'
    ];

    protected $table = 'employees';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'emp_id';

}
